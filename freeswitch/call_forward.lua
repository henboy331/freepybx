require "luasql.postgres"

env = assert(luasql.postgres())
con = assert(env:connect("dbname=freepybx user=regularex password=3v3lyn555 host=127.0.0.1"))

--[[ Custom Logging makes it easier to troubleshoot or remove.]]--
function log(logval)
    freeswitch.consoleLog("NOTICE", "LUA ROUTE.LUA ----------------->" .. tostring(logval) .. "<------------- \n")
end

--[[ rows iterator object ]]--
function rows(sql)
    local cursor = assert (con:execute (sql))
    return function ()
        return cursor:fetch()
    end
end

--[[ Tidy-up connections. ]]--
function session_hangup_hook()
    freeswitch.consoleLog("NOTICE", "Session hangup hook...\n")
    con:close()
end

--[[ String "startswith" helper function. ]]--
function startswith(sbig, slittle)
    if type(slittle) == "table" then
        for k,v in ipairs(slittle) do
            if string.sub(sbig, 1, string.len(v)) == v then
                return true
            end
        end
        return false
    end
    return string.sub(sbig, 1, string.len(slittle)) == slittle
end

--[[ Split helper function. ]]--
function split(str, pat)
    local t = {}
    local fpat = "(.-)" .. pat
    local last_end = 1
    local s, e, cap = str:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= "" then
            table.insert(t,cap)
        end
        last_end = e+1
        s, e, cap = str:find(fpat, last_end)
    end
    if last_end <= #str then
        cap = str:sub(last_end)
        table.insert(t, cap)
    end
    return t
end

--[[ DID route lookup. ]]--
function get_route_by_did(did)
    db = assert(con:execute(string.format("SELECT pbx_routes.* " ..
            "FROM pbx_routes " ..
            "INNER JOIN pbx_dids ON pbx_routes.id = pbx_dids.pbx_route_id " ..
            "WHERE pbx_dids.did = '%s'", tostring(did))))
    return db:fetch({}, "a")
end


function send_route(route, context)
    log(route["name"])
    if route["pbx_route_type_id"] == "1" then
        check_exists_available(route["name"], context)
    else
        log("No route type ID associated with call.")
    end
end

--[[
    -- Global Chunk --
 ]]

username = session:getVariable("username")
context = session:getVariable("context")
domain = session:getVariable("domain")
is_forward = session:getVariable("is_forward")


-- IN
if is_forward then

else


end

